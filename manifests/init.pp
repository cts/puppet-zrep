#
class zrep {
  package { 'ksh': }

  file { '/var/lib/mysql/dump':
    ensure  => directory,
    mode    => '0700',
    owner   => 'root',
    group   => 'root',
  }

  file { '/usr/local/bin/zrep':
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "puppet:///modules/${module_name}/zrep",
  }

  file { '/usr/local/bin/backup.sh':
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "puppet:///modules/${module_name}/backup.sh",
  }

  cron { 'mysql dump and zrep stuff':
    command => 'PATH=/sbin:$PATH /usr/local/bin/backup.sh',
    user    => 'root',
    hour    => [10,14,16],
    minute  => 0,
    require => [
      File['/usr/local/bin/backup.sh'],
      File['/usr/local/bin/zrep'],
      Exec['zrep init mysql'],
      Exec['zrep init git'],
    ],
  }

  exec { 'zrep init mysql':
    command => '/usr/local/bin/zrep init gitlab/mysql gitlabdr.icer.msu.edu msugitlabdr/mysql',
    unless  => '/usr/local/bin/zrep status -v | grep "gitlab/mysql"',
    require => File['/usr/local/bin/zrep'],
  }
  exec { 'zrep init git':
    command => '/usr/local/bin/zrep init gitlab/git gitlabdr.icer.msu.edu msugitlabdr/git',
    unless  => '/usr/local/bin/zrep status -v | grep "gitlab/git"',
    require => File['/usr/local/bin/zrep'],
  }
}

